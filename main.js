console.log('check prime number');
console.time('checkPrime');
function checkPrime(n) {
let divisor = 2;
while(n>divisor){
    if(n%divisor === 0){
        return false;
    }else {

    divisor++;
    }
}
return true;
}


console.timeEnd('checkPrime');
console.log(checkPrime(27));


console.log('another attempt to check prime');
console.time('checkPrime1');
function checkPrime1(n) {
    if(n<2){
        return false;
    }else {
        for(let i=2; i<n; i++){
            if(n%i === 0){

                return false;
               
            }
        }
        return true;
    }
}

console.timeEnd('checkPrime1');
console.log(checkPrime1(27));

console.log('above one solution is having o(n) complexity. let us try to reduce it to o(sqIn)');

console.time('checkPrime2');
function checkPrime2(n){
        for(let i=2,  s= Math.sqrt(n); i<=s; i++){
            console.log(n%i);
            if(n%i === 0){
                return false;
            }
        } 

        return n>1;
    
}


console.timeEnd('checkPrime2');
console.log(checkPrime2(67));

console.log('find prime factor');

console.time('findPrime');
function findPrime(n){
    var factors = [];
    var divisor = 2;
    while(n>2) {
        if(n%divisor ===0 ){
            n = n/divisor;
       factors.push(divisor)
        }else{
            divisor++;
        }
    }
    console.log(factors);
    return factors;
}
console.timeEnd('findPrime');
findPrime(69);



console.time('findPrime1');
function findPrime1(n){
    var str = "0";
for(i=1; i<=n; i++){
    if(n%i ===0){
        str +=','+ i;
    }
}
return str;
}
console.log(findPrime1(45));

console.timeEnd('findPrime1');


console.log('findPrime2 is of Math.squrt(n) complexity');

console.time('findPrime2');
function findPrime2(n){
    let output = [];
    let s = Math.sqrt(n); 
    for(i=0;i<=s; i++){
        console.log(n%i);
        if(n%i === 0){
          output.push(i);
        
        if(i !== s){
            output.push(n/i);
        }
        }

    }
    if(output.indexOf(n) === -1){
        output.push(n);
    }

    
    console.log(output);
}

console.timeEnd('findPrime2');
 findPrime2(32);



 console.log('Find Fibbonaci number');

 console.time('fibbo');
 function fibbo(n) {
let ar = [0,1];
for(let i=2;i<n;i++){
    ar.push(ar[i-1]+ar[i-2]);
}

console.log(ar);
 }


 console.timeEnd('fibbo');
 fibbo(10);



 console.time('fibbo1');
 function fibbo1(n){
    if(n == 1){
         return [0,1];
     }else {
          var prevSeq = fibbo1(n-1);
          if(prevSeq)
           prevSeq.push(prevSeq[prevSeq.length-1]+prevSeq[prevSeq.length-2]);
           return prevSeq;
     }
 }


 console.timeEnd('fibbo1');

 console.log(fibbo1(10));

 console.time('fibonacciOptimised');
 function fibonacciOptimised(n,map){
    if (n < 0)
    throw 'n must be >= 0';
  if (n === 0)
    return 0;
  if (n === 1)
    return 1;
  if (!map)
    map = {};
  if (!map[n])
    map[n] = fibonacciOptimised(n - 1, map) + fibonacciOptimised(n - 2, map);
  console.log(map)
  return map[n];

}
 console.timeEnd('fibonacciOptimised');

 console.log(fibonacciOptimised(10));

console.log('find GCD of two number');


console.time('gcd');
function gcd(a,b){
    if(typeof a !== 'number' || typeof b !=='number'){
        return false;
    }else{
        x = Math.abs(a);
        y = Math.abs(b);
        while (y) {
            var t = y;
            y = x%y;    
            x = t;
        }
        return x;
    }
}

console.timeEnd('gcd');
console.log(gcd(48,56));


console.time('gcd');



function gcd1(a,b){
if (b==0) 
    return a;
   return gcd1(b,a%b);   

}

console.timeEnd('gcd1');
console.log(gcd1(4,8));


console.log('remove duplicates from an array');


console.time('removeDuplicates');
function removeDuplicates(array){
    let mod = [];
    for(let i=0; i<array.length; i++){
        if(mod.indexOf(array[i]) == -1){
            mod.push(array[i]);
        }
    }
    console.log(mod);
    return mod;
}

removeDuplicates([3,5,6,4,3,4]);


console.timeEnd('removeDuplicates');

function removeDuplicates1(array) {
    var seen = {};
    var retArray = [];
    for(let i=0; i<array.length; i++){
        seen[i] = array[i];

     if (!(array[i] in seen)) {
        retArray.push(array[i]);
        seen[array[i]] = true;
     }
    }
    console.log(seen);
    console.log(retArray);
    return retArray;
}

removeDuplicates1([3,5,6,4,3,4]);


function removeDuplicates2(array){
    var seen = {};
    return array.filter((item)=>{
    return seen.hasOwnProperty(item)?false:(seen[item]=true);
    })
}

console.log(removeDuplicates2([2,4,5,6,72,3,4,5]));


console.log('merge two sorted array');

function mergeSortedArray(array1,array2) {
    let mergedArray = [];
    let array1Index = 0;
    let array2Index = 0;
    let currentIndex = 0;
    let isarray1Depleted = array1Index>=array1.length;
    let isarray2Depleted = array2Index>=array2.length;
    while (currentIndex<(array1.length+array2.length)) {
        let unmerged1 = array1[array1Index];
        let unmerged2 = array2[array2Index];
        let isArray1Depleted = array1Index>=array1.length;
        let isArray2Depleted = array2Index>=array2.length;
      if(!isArray1Depleted && (isArray2Depleted || unmerged1<unmerged2)) {
        mergedArray[currentIndex] = unmerged1;
          array1Index++;

      }else {

        mergedArray[currentIndex] = unmerged2;
        array2Index++;
      }
      currentIndex++;        
    }

    return mergedArray;

}

console.log(mergeSortedArray([1,3,9],[6,7,8]));



function mergeSortedArray1(a,b){
    var merged = [], 
    aElm = a[0],
    bElm = b[0],
    i = 1,
    j = 1;

if(a.length ==0)
  return b;
if(b.length ==0)
  return a;
/* 
if aElm or bElm exists we will insert to merged array
(will go inside while loop)
 to insert: aElm exists and bElm doesn't exists
           or both exists and aElm < bElm
  this is the critical part of the example            
*/
while(aElm || bElm){
 if((aElm && !bElm) || aElm < bElm){
   merged.push(aElm);
   aElm = a[i++];
 }   
 else {
   merged.push(bElm);
   bElm = b[j++];
 }
}
return merged;
}

console.log(mergeSortedArray1([1,3,9],[6,7,8]));


function swap(a,b){
   var a,b;
    b= a+b;
    a=b-a;
    b=b-a;
    console.log(a);
    console.log(b);
    return 'value of a:'+a+'value of b:'+b;
}
console.log(swap(4,9));



function reverseStr(str){
    var retstr = '';
    for(let i = str.length-1; i>=0; i-- ){
        retstr+=str[i];
    }
    return retstr;
}

console.log(reverseStr('HELLO'));


function reverseStr1(str){
    var o =[];
    for (let index = 0; index <= str.length; index++) {
        const element = str[str.length-index];

        o.push(str.charAt(str.length-index));
       // return o.join('');
        
    }
    console.log(o.join(''));
    return o.join('');
}

reverseStr1('HELLO');

function reverseStr2(str) {
    return str.split('').reverse().join();
}

function recursionReverseStr(str){
    if (str == '') {
        return '';
        
    } else {
        return recursionReverseStr(str.substr(1))+str.charAt(0);
        
    }
}

console.log(recursionReverseStr('HELLO'));


function reverseWord(str) {
    var reverseStr = '';
    var wordsArr = str.split(' ');
    for(let i = wordsArr.length-1; i>=0; i--){
        reverseStr+=wordsArr[i]+' ';
    }
    console.log(reverseStr);
    return reverseStr.trim();
}

reverseWord('This is cool');

function reverseWordInPlace(str) {
    return str.split(' ').reverse().split('').reverse().join();
}

    function firstNonRepeatingChar(str){
        var len = str.length,currentChar, characterCount = {}, newStr= [];
        for(let i =0; i<len; i++){
            currentChar = str[i];
            if (characterCount[currentChar]) {
                characterCount[currentChar]++;   
            } else {
                characterCount[currentChar] = 1;
            }
        }
    console.log(characterCount);
        for(var i in characterCount){
            if(characterCount[i] == 1){
                newStr.push(i);
                console.log(newStr);
              //  return i;
            }
        }
        console.log(newStr.join(''));
    }

    console.log(firstNonRepeatingChar('this is dope bro, I swear'));


    console.log(isPalindrome('madam'));

    function isPalindrome(str){
        var i, len = str.length;
        for(i =0; i<len/2; i++){
            console.log('lhs'+str[i]);
            console.log('rhs'+str[len-1-i]);
          if (str[i]!== str[len -1 -i])
             return false;
        }
        return true;
      }


      console.log('find missing number');

      function findMissing(array){
        let length = array.length+1;
        let expected = length*(length+1)/2;
        let sum = array.reduce((a,b)=>a+b);
        return expected-sum;
      }

      console.log(findMissing([1,2,3,4,5,7]));

     console.time('findMissing1');
      function findMissing1(array){
        for(let i=0 ; i<array.length; i++){
            if (array[i+1]-array[i] ==2) {
                return array[i]+1;
            }
        }
      }
  console.timeEnd('findMissing1')

 console.log(findMissing1([1,2,3,4,5,7]));

 function sumFinder(arr,sum){
     for(let i=0; i<arr.length-1; i++){
         for(let j=0; j<arr.length; j++){
            if(arr[i]+arr[j] == sum){
                return [arr[i],arr[j]];
            }
         }
     }

 }


 console.log(sumFinder([2,3,4,7],6));

 function sumFinder1(arr,sum){
     var differ = {}, len = arr.length, substract;
     for(var i=0; i<len; i++){
         substract = sum-arr[i];
         console.log(substract);
         if (differ[substract]) {
             console.log(substract)
             return true;
         }else{
             return differ[substract] = true;
         }
     }

     console.log(differ);
     return false;
 }



 console.log(sumFinder1([2,3,4,7],6));


 function sumOfNumbers(array){
     let biggest = -Infinity;
     let nextBiggest = -Infinity;
     for(let i=0; i<array.length; i++){
         if(array[i]>biggest){
             nextBiggest = biggest
             biggest = array[i];
         }else if(array[i]<biggest && array[i]>nextBiggest){
             nextBiggest = array[i];
         }
     }
     return biggest+nextBiggest;
 }
 console.log(sumOfNumbers([5,6,3,8]));


function count(n){
     let count = 0;
  while (n>0) {
      count += Math.floor(n/10);
      n = n/10; 
  }


  return count;
 }

 console.log(count(50))